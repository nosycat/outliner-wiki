# Outliners, note-taking apps and everything

> An outliner (or outline processor) is a specialized type of text editor (word processor) used to create and edit outlines, which are text files which have a tree structure, for organization. Textual information is contained in discrete sections called "nodes", which are arranged according to their topic–subtopic (parent–child) relationships, like the members of a family tree. When loaded into an outliner, an outline may be collapsed or expanded to display as few or as many levels as desired.
>
> &mdash; [From Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Outliner)

Outliners overlap with note-taking apps, often by a lot. Also many of them support time tracking, task lists and so on.

This document provides a brief look at well-known, popular or otherwise relevant software in these categories. For others, see the [companion file](companion.md).

## Outliners

- The 800-pound gorilla of outliners is [Org Mode](https://orgmode.org/).

Some are only kept around by nostalgia:

- [hnb](http://hnb.sourceforge.net/) <q>a curses program to structure many kinds of data in one place</q> ancient, abandoned and awkward

### Unusual

- [TreeSheets](https://strlen.com/treesheets/) <q>Free Form Data Organizer (Hierarchical Spreadsheet)</q> a pretty cool concept it turns out, in active development as of late 2023
- [TreeLine](https://treeline.bellz.org/) is arguably more of a personal database (see review in other file) with nested records; still developed as of late 2020

### Mobile

- [Orgzly](http://www.orgzly.com/): Outliner using org files (see below) to import/export data. It helps to know a few things about this file format, for example how to set a notebook title and what drawers are. Orgzly focused on agenda features, but is equally good for notes or planning.

### File formats

- Org files are the underlying format of Org Mode. While complex, a decent subset can be parsed easily enough. Compatible apps include Orgzly (and others not mentioned here).
- [OPML](http://opml.org/) is an old and much maligned format, nowadays mostly used by RSS readers to exchange lists of feeds; `hnb` can use it.

### Links

- [OutlinerSoftware.com](https://outlinersoftware.com/), an active forum (as of mid-2022)
- The [OrgDown](https://gitlab.com/publicvoit/orgdown) initiative lists many apps with support for org files.

## Note-taking apps

> Note-taking (sometimes written as notetaking or note taking) is the practice of recording information from different sources and platforms. By taking notes, the writer records the essence of the information, freeing their mind from having to recall everything.
> 
> &mdash; [From Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Note-taking)

While any text editor can be used to take notes, the point is doing it in such a way that notes can be easily navigated and organized afterwards.

- [Zim](https://zim-wiki.org/) <q>A Desktop Wiki</q> that's also an outliner; quirky but friendly, and very flexible. Among the most popular apps here.

### Web-based

- [TiddlyWiki](https://tiddlywiki.com/) <q>a non-linear personal web notebook</q>
  - [Feather Wiki](https://feather.wiki/) <q>like TiddlyWiki but with the smallest file size possible</q>
- [Domino](https://kool.tools/domino/#0,0) <q>a tool for collaging thoughts</q>

Hosted services:

- [rwtxt](https://rwtxt.com/public): "store any text online for easy sharing"
- [standard definition notes](https://sdnotes.com/faq), "Maybe the world's easiest website maker?" 

### Mobile

- [Markor](https://gsantner.net/project/markor.html) <q>Markdown Editor, todo.txt, Android app</q>
- [Tiddloid](https://github.com/donmor/Tiddloid) <q>an app to work with locally stored TiddlyWikis</q>
- [jtx Board](https://jtx.techbee.at/) <q>journals, notes & tasks for Android | iCalendar compliant | CalDAV sync through DAVx5</q>

Also there's a set of mobile apps for taking notes that all seem to work the same way, and only differ in the amount of features they offer. All are open source and available on F-Droid. Does this have a name?

- Notally
- Another Notes App
- [Quillpad](https://quillpad.github.io/)

They all seem based on a grid of brief text notes mixed with to-do lists; any of them can have a title, tags and so on, depending. They can be pinned, too. The concept works best with larger screens.

They look similar to [Carnet](https://getcarnet.app/), a large multiplatform app.

### Links

- [Digital Gardening Tools and Resources](https://github.com/MaggieAppleton/digital-gardeners)
- [Awesome Knowledge Management](https://github.com/brettkromkamp/awesome-knowledge-management)

## License

This text is under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
