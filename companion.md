# Outliner wiki companion

This document is part of the outliner wiki (for certain values of "wiki"). Its purpose is to list lesser known or related apps, so as to keep the main page clean.

Outliners, note-taking apps or other software with similar uses belong here.

## Outliners

- [GJots2](http://bhepple.freeshell.org/gjots/) and [FeatherNotes](https://github.com/tsujan/feathernotes) are two small outliners available in Debian that save their notebooks to single files, in proprietary (if text-based) formats. Both are clunky and limited:
  - GJots2 treats notes as plain text but its own file format is human-readable;
  - FeatherNotes has a rich text editor (with few options), but no data export.
- [OutWiker](https://jenyay.net/Outwiker/English) is a mix between outliners and wikis (see below), running on the desktop. Looks very similar to Zim, except for the lack of a rich text editor. Via @bouncepaw.

### Using common formats

The big problem with most outliners is that they insist on using their own ad-hoc file formats instead of common options. This author's outliners are a reaction to this trend:

- [Scrunch Edit](https://ctrl-c.club/~nttp/toys/scrunch/), a two-pane outliner for Markdown and org files; treats both as plain text (apart from headings);
- [OutNoted](https://ctrl-c.club/~nttp/toys/outnoted/), an outline note-taking editor; can use org files, Markdown or OPML, but only in specific ways.

A few mobile outliners use org files as well.

### Wikis

- Wiki engines like [Feather Wiki](https://feather.wiki/) and [Pepperminty Wiki](https://peppermint.mooncarrot.space/) can show a tree of pages in the sidebar, thus acting much like outliners. These two also use Markdown as it happens.

## Text User Interface

It's hard to find good apps in this category, because most use a command line, which misses the point.

- [TUI Journal](https://github.com/AmmarAbouZor/tui-journal) <q>Your journal app if you live in a terminal</q>
- [JourNote](https://codeberg.org/nosycat/journote): this author's own take on the matter.
- [Mayhem](https://github.com/BOTbkcd/mayhem) <q>A minimal TUI based task tracker</q>

## Old

- [Tomboy](https://wiki.gnome.org/Apps/Tomboy) was once *the* note-taking app, but it fell from grace as Linux distributions dropped Mono one by one. It was later remade in C++ as [Gnote](https://wiki.gnome.org/Apps/Gnote), but most users had moved on.

## Potential

- [Twine][] might be good for taking notes
- Collage website makers like [HotGlue][] or [Multiverse][] could work too
- So could [Shaarli][], a bookmark manager that supports notes with Markdown and tags
- [Decker][] also supports text, images, tables and at least two different ways to link between cards

[Twine]: https://twinery.org/
[HotGlue]: https://www.hotglue.me/
[Multiverse]: https://multiverse.plus/
[Shaarli]: https://github.com/shaarli/Shaarli
[Decker]: https://beyondloom.com/decker/

## Links

- [An outliner done right](https://felix.plesoianu.ro/blog/outliner-done-right.html): this author's review of TreeLine
- [The outline of a game](https://notimetoplay.org/engines/game-outlines.html) an opinion piece, also by this author

## License

This text is under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
